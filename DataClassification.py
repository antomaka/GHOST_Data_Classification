from pymongo import MongoClient
import numpy as np
import random
from sklearn.svm import SVC
from sklearn import linear_model
from sklearn import datasets
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import tree
from sklearn import svm



def template_extraction(flows, required_fields):
	values = flows.find()
	flow_list = []

	for value in values:
		flow_list.append(value)

	n = len(flow_list)
	m = len(required_fields)
	templates = np.zeros(shape=(n,m))

	for i in range(0,n):
		d = flow_list[i]
		keys = d.keys()
		j=0
		for key,value in d.items():
			if key in required_fields:
				templates[i][j]=value
				#print(templates[i][j])
				j=j+1
	return templates

#MANUAL ANNOTATION OF CLASSES
def manual_annotation(templates):
	n = len(templates)
	manual_classes = np.zeros(shape=(n))
	for i in range(0,n):
		manual_classes[i] = random.randint(0,1)
		#print(manual_classes[i])
	return manual_classes


#TRAINING OF CLASSIFIERS
def svc_classification(x, y):
	svc = SVC()
	svc = svc.fit(x, y)
	return svc

def GaussianNaiveBayes_classification(x, y):
	gnb = GaussianNB()
	gnb = gnb.fit(x, y)
	return gnb

def BernouliNaiveBayes_classification(x, y):
	bernnb = BernoulliNB()
	bernnb = bernnb.fit(x, y)
	return bernnb

def MultinomialNaiveBayes_classification(x, y):
	mulnb = MultinomialNB()
	mulnb = mulnb.fit(x, y)
	return mulnb

def RandomForest_classification(x, y):
	randforest = RandomForestClassifier(max_depth=2, random_state=0)
	randforest = randforest.fit(x, y)
	return randforest

def DecisionTree_classification(x, y):
	dtree = tree.DecisionTreeClassifier()
	dtree = dtree.fit(x, y)
	return dtree

def SupportVectorMachine_classivication(x, y):
	supvecmac = svm.SVC()
	supvecmac = supvecmac.fit(x, y)
	return supvecmac

#CLASSIFICATION OF INCOMING FLOWS
def data_classification(incoming_flows, method):
	n=len(incoming_flows)
	classes = np.zeros(shape=(n))
	for i in range(0,n):
		classes[i] = method.predict([incoming_flows[i]])
		#print(classes[i])
	return classes


client = MongoClient()
db = client.ndfa_database


#-----------------IP PACKETS-------------------

eth_required_fields=[u'max_iat_a', u'stdv_iat_b', u'min_load_b', u'max_iat_b', u'tb_b', u'bytes_a', u'sfp_a', u'sfp_b', u'min_iat_b', u'min_iat_a', u'port_b', u'port_a', u'min_load_a', u'stdv_iat_a', u'ts_end', u'bytes_b', u'mean_iat_b', u'mean_iat_a', u'tb_a', u'max_load_b', u'mean_load_a', u'mean_load_b', u'max_load_a', u'tran_prot', u'stdv_load_a', u'stdv_load_b', u'packets_b', u'packets_a', u'ts_start']


#READ FROM DATABASE
flows = db.eth0_flows

#TEMPLATE EXTRACTION
templates_eth = template_extraction(flows, eth_required_fields)

#MANUAL ANNOTATION OF CLASSES
manual_classes_eth = manual_annotation(templates_eth)

#for i in range(0,len(templates_eth)):
	#print(templates_eth[i])

#TRAINING OF CLASSIFIERS
svc_eth = svc_classification(templates_eth, manual_classes_eth)
gnb_eth = GaussianNaiveBayes_classification(templates_eth, manual_classes_eth)
bernnb_eth = BernouliNaiveBayes_classification(templates_eth, manual_classes_eth)
mulnb_eth = MultinomialNaiveBayes_classification(templates_eth, manual_classes_eth)
randforest_eth = RandomForest_classification(templates_eth, manual_classes_eth)
dtree_eth = DecisionTree_classification(templates_eth, manual_classes_eth)
svm_eth = SupportVectorMachine_classivication(templates_eth, manual_classes_eth)


#READING NEW FLOWS
flows = db.eth0_flows
incoming_flows = template_extraction(flows, eth_required_fields)

#CLASSIFICATION OF FLOWS BY SOME SELECTED METHOD
classes_svc = data_classification(incoming_flows, svc_eth)
classes_gnb = data_classification(incoming_flows, gnb_eth)
classes_bernnb = data_classification(incoming_flows, bernnb_eth)
classes_mulnb = data_classification(incoming_flows, mulnb_eth)
classes_randforest = data_classification(incoming_flows, randforest_eth)
classes_dtree = data_classification(incoming_flows, dtree_eth)
classes_svm = data_classification(incoming_flows, svm_eth)



#-----------------BLUETOOTH PACKETS-------------------

bt_required_fields=[u'number_of_packets', u'start_time', u'min_size', u'duration', u'stop_time', u'batch_id', u'max_size', u'average_size']

#READ FROM DATABASE
batches = db.bt_batches

#TEMPLATE EXTRACTION
templates_bt = template_extraction(batches, bt_required_fields)

#MANUAL ANNOTATION OF CLASSES
manual_classes_bt = manual_annotation(templates_bt)

#TRAINING OF CLASSIFIERS
svc_bt = svc_classification(templates_bt, manual_classes_bt)
gnb_bt = GaussianNaiveBayes_classification(templates_bt, manual_classes_bt)
bernnb_bt = BernouliNaiveBayes_classification(templates_bt, manual_classes_bt)
mulnb_bt = MultinomialNaiveBayes_classification(templates_bt, manual_classes_bt)
svm_bt = SupportVectorMachine_classivication(templates_bt, manual_classes_bt)


#READING NEW batches
batches = db.bt_batches
incoming_batches = template_extraction(batches, bt_required_fields)

#CLASSIFICATION OF BATCHES BY SOME SELECTED METHOD
classes_svc = data_classification(incoming_batches, svc_bt)
classes_gnb = data_classification(incoming_batches, gnb_bt)
classes_bernnb = data_classification(incoming_batches, bernnb_bt)
classes_mulnb = data_classification(incoming_batches, mulnb_bt)
classes_svm = data_classification(incoming_batches, svm_bt)

